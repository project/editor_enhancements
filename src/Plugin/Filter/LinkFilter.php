<?php

declare(strict_types=1);

namespace Drupal\editor_enhancements\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to alter Links.
 *
 * @Filter(
 *   id = "editor_enhancements_link",
 *   title = @Translation("Editor Enhancements Link Filter"),
 *   description = @Translation("Add target _balnk to full-path links."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "open_link_new_window" = "boolean",
 *   },
 *   weight = 100
 * )
 */
class LinkFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form["open_link_new_window"] = [
      '#type' => 'checkbox',
      '#title' => "Open link in new window",
      '#default_value' => boolval($this->settings["open_link_new_window"]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);
    $html_dom = Html::load($text);
    $xpath = new \DOMXPath($html_dom);
    /** @var \DOMElement $node */
    foreach ($xpath->query('//a') as $node) {
      if (!$node->hasAttribute('href')) {
        continue;
      }
      $href = $node->getAttribute('href');
      if (str_starts_with($href, 'http') && boolval($this->settings["open_link_new_window"])) {
        $node->setAttribute("target", "_blank");
      }
    }

    return $result->setProcessedText(Html::serialize($html_dom));
  }

}
