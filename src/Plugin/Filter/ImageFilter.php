<?php

declare(strict_types=1);

namespace Drupal\editor_enhancements\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a filter to alter images.
 *
 * Convert src paths to full URLs and provide a way to remove img attibutes.
 *
 * @Filter(
 *   id = "editor_enhancements_image",
 *   title = @Translation("Editor Enhancements Image Filter"),
 *   description = @Translation("Convert image src paths to full URLs and remove attributes."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "remove_attribute_loading" = "boolean",
 *     "remove_attribute_width" = "boolean",
 *     "remove_attribute_height" = "boolean",
 *   },
 *   weight = 100
 * )
 */
class ImageFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a \Drupal\editor\Plugin\Filter\EditorFileReference object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    $this->requestStack = $request_stack;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    foreach ($this->getImageAttributes() as $attribute) {
      $form["remove_attribute_$attribute"] = [
        '#type' => 'checkbox',
        '#title' => "Remove $attribute attribute",
        '#default_value' => boolval($this->settings["remove_attribute_$attribute"]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);
    $html_dom = Html::load($text);
    $requestSchemeAndHttpHost = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $xpath = new \DOMXPath($html_dom);
    /** @var \DOMElement $node */
    foreach ($xpath->query('//img') as $node) {
      if (!$node->hasAttribute('src')) {
        continue;
      }
      foreach ($this->getImageAttributes() as $attribute) {
        if (boolval($this->settings["remove_attribute_$attribute"])) {
          $node->removeAttribute($attribute);
        }
      }
      $imagePath = $node->getAttribute('src');
      if (str_starts_with($imagePath, 'http')) {
        continue;
      }
      $node->setAttribute("src", "$requestSchemeAndHttpHost$imagePath");
    }

    return $result->setProcessedText(Html::serialize($html_dom));
  }

  /**
   * Gets plugins setting.
   */
  public function getSetting($name) {
    return $this->settings[$name];
  }

  /**
   * Get image sttributes.
   */
  protected function getImageAttributes() {
    return ['loading', 'width', 'height'];
  }

}
